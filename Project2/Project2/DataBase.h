#include <iostream>
#include <string>
#include <Windows.h>
#include "SQLite.h"
#include "Message.h"
#pragma once

using namespace std;
class SQLite;

class DataBase
{
private:

public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool isUserAndPassMatch(Message*);
	bool getUserByName(string UserName);
};