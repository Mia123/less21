#pragma once
#include <iostream>
#include "User.h"
#include "Helper.h"
#include <WinSock2.h>
#include <Windows.h>
#include <condition_variable>
#include <algorithm>

using namespace std;

class Game
{
private:
	//std::map<string, int> _results;
	int _currentTurnAnswers;
public:
	Game(const vector<User*>& players,int questionsNo/*DataBase& db*/);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser, );
};
