#include "Message.h"

//C' tor
Message::Message(MessageType code, SOCKET socket, User * user) : _code(code), _msgSocket(socket), _optor(user)
{
}

//D'tor
Message::~Message()
{
}

User * Message::getUser()
{
	return _optor;
}

SOCKET Message::getSocket()
{
	return _msgSocket;
}

MessageType Message::getCode()
{
	return _code;
}

condition_variable * Message::getCond()
{
	return &_cond;
}

void Message::releaseMessage()
{
	_cond.notify_all();
}
