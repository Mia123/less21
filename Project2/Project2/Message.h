#pragma once
#include <iostream>
#include "User.h"
#include "Helper.h"
#include <WinSock2.h>
#include <Windows.h>
#include <condition_variable>

class Message
{
private:
	User* _optor;
	SOCKET _msgSocket;
	MessageType _code;
	condition_variable _cond;
public:
	Message(MessageType code, SOCKET socket, User* user);
	~Message();
	User* getUser();
	SOCKET getSocket();
	MessageType getCode();
	condition_variable* getCond();
	void releaseMessage();
	
};