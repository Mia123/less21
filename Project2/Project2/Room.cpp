#include "Room.h"
#include "User.h"

#define JOIN_FAILED 1102
#define FULL_ROOM 1101
#define USERS_IN_ROOM 108
#define JOIN_SUCCESSED 1100
#define CLOSE_ROOM 116
#define LEAVE_ROOM 112
#define CONNECTED_USERS 207


//C'tor
Room::Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionsNo;
	_questionTime = questionTime;
	_users.push_back(_admin);
}


//Output: Vector of users.
vector<User*> Room::getUsers()
{
	return _users;
}

//Output: room questionNo.
int Room::getQuestionsNo()
{
	return _questionsNo;
}

//Output: room id.
int Room::getId()
{
	return _id;
}

//Output: room name.
string Room::getName()
{
	return _name;
}


//Input: new user to add the room. 
//Output: true if new user were added.
bool Room::joinRoom(User* newUser)
{
	bool flag = true;
	if (_maxUsers > _users.size + 1)
	{
		_users.push_back(newUser);
		sendMessage(newUser, JOIN_SUCCESSED);
		sendMessage(getUsersListMessage());
	}
	else   //Join Falid .
	{
		flag = false;
		sendMessage(newUser, FULL_ROOM);
	}
	return flag;
}



//Input: user to remove from the room.
void Room::leaveRoom(User* userToRemove)
{
	_users.erase(std::remove(_users.begin(), _users.end(), userToRemove), _users.end());
	sendMessage(userToRemove, LEAVE_ROOM);
	sendMessage(getUsersListMessage());
}

//Input: send a STRING to all connected users.
void Room::sendMessage(string st)
{
	for (auto it = _users.begin(); it != _users.end(); ++it)
		it->send(st);
}

//send STRING to USER.
void Room::sendMessage(User* us, string st)
{
	us->send(st);
}


//Buliding the protocol according the given file.
//108 numberOfUsers ## username ## username  or 1080 if the room doesnot exists. 
string Room::getUsersListMessage()
{
	string returnedValue = "108";
	string name;
	if (this)
	{
		for (auto it = _users.begin(); it != _users.end(); ++it)
			name = it->getName();
		returnedValue += _users.size() + "##" + name.size + "##" + name;
	}
	else
		returnedValue += "0";
	return returnedValue;
}

//Output: -1 if closed falied, id otherwise.
int Room::closeRoom(User* user)
{
	int returnValue = -1;
	if (user == _admin && user->closeRoom() != -1)
	{
		for (std::vector<User*>::iterator i = _users.begin(); i != _users.end(); ++i)
		{
			i->User::send(CLOSE_ROOM);
			if (i != _admin)
				i->clearRoom();
		}
		returnValue = _id;
	}
	return returnValue;
}



