#include "Server.h"
#include "Helper.h"
#include "User.h"
#include "Message.h"
#include <exception>
#include <mutex>
#include <vector>
#include <condition_variable>
#include <fstream>
#include <thread>
#include <iostream>
#include <string>

using namespace std;

condition_variable cond;
mutex lck;


/**
C'tor
*/
Server::Server() : _current(NULL)
{
	DataBase* db;

	// notice that we step out to the global namespace
	// for the resolution of the function socket
	
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_roomIdSequence = 0;
	_serverSocket = ::socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 
	

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_serverSocket);
		while (!_users.empty())
		{
			User* user = _users.front();
			delete user;
			_users.pop();
		}

		while (!_messages.empty())
		{
			Message* msg = _messages.front();
			delete msg;
			_messages.pop();
		}
	}
	catch (...) {}
}

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;
	
	std::thread handleThread(&Server::handleMessages, this);
	handleThread.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}

/**
Gets client that will connect to the server.
Creats new thred for the socket.
*/
void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	std::thread handle(&Server::clientHandler, this, client_socket);
	handle.detach();
}

/**
Recive a message from a socket and translate it to a readable message.
*/
void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		int code =Helper::getMessageTypeCode(clientSocket);
		int length =Helper::getIntPartFromSocket(clientSocket,2);
		string name = Helper::getStringPartFromSocket(clientSocket, length);
		length = Helper::getIntPartFromSocket(clientSocket, 2);
		string password = Helper::getStringPartFromSocket(clientSocket, length);
		User* user = new User(name,password, clientSocket);
		int b= Helper::checkLogin(user, _users);
		if (_current == NULL)
		{
			_current = user;
			user->setPosition(1);
		}

		else
		{
			user->setPosition(_users.size() + 2);
			_users.push(user);
		}

		sendUpdateToClient(user);
		
		while (true)
		{
			code = Helper::getMessageTypeCode(clientSocket);
			MessageType codeType = (MessageType)code;
			Message* message = new Message(codeType, clientSocket, user);
			_messages.push(message);
			cond.notify_all();
			waitToFinish(message);
		}
		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket); 
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}


}

/**
*/
void Server::handleMessages()
{
	while(true)
	{
		unique_lock<mutex> lock(lck);
		cond.wait(lock, [&]() {return !_messages.empty(); });
		
		while (!_messages.empty())
		{

			Message* currentMessage = _messages.front();
			
			switch (currentMessage->getCode())
			{
			case SIGH_IN:
				sighIn();
				sendAll();
				break;
			case SIGH_OUT:
				disconnect(currentMessage->getUser());
				sendAll();
				break;
			case SIGH_UP:
				sighUp();
				sendAll();
				break;
			case LIST_R_E:
				listRooms();
				sendAll();
				break;
			case LIST_R_ID:
				listUsers();
				sendAll();
				break;
			case JOIN_E_ROOM:
				_current.joinRoom();
				sendAll();
				break;
			case LEAVE_ROOM:
				_current.leaveRoom();
				sendAll();
				break;
			case CREATE_NEW_R:
				_current.newRoom();
				sendAll();
				break;
			case CLOSE_ROOM:
				_current.closeRoom();
				sendAll();
				break;
			case START_GAME:
				_current.start();
				sendAll();
				break;
			case ANSWER:
				_current.checkAnswer();
				sendAll();
				break;
			case QUIT_ROOM:
				_current.quitRoom();
				sendAll();
				break;
			case BEST_SCORES:
				_current.bestScore();
				sendAll();
				break;
			case STATUS:
				_current.myStatus();
				sendAll();
				break;
			case QUIT_APLICTION:
				sendAll();
				break;
			default:
				break;
			}
			currentMessage->releaseMessage();
			delete currentMessage;
			_messages.pop();
		}
	}
}

void Server::sendUpdateToClient(User* user)
{
	ifstream file("data.txt");
	std::string content((std::istreambuf_iterator<char>(file)),(std::istreambuf_iterator<char>()));

	string cUserName = "";
	string nUserName = "";

	if (_current != NULL) 
	{
		cUserName = _current->getName();
	}

	if (!_users.empty())
	{
		nUserName = _users.front()->getName();
	}

	Helper::sendUpdateMessageToClient(user->getSocket(), content, cUserName, nUserName, user->getPosition());
	
}

void Server::sendAll()
{
	std::queue<User*> users;
	if (_current)
	{
		sendUpdateToClient(_current);
	}
	
	vector<User*> usrList;
	while (!_users.empty())
	{
		User* currentUser = _users.front();
		_users.pop();
		users.push(currentUser);
		usrList.push_back(currentUser);
		
	}
	ret(users, _users);

	vector<User*>::iterator uiterator = usrList.begin();
	for (; uiterator != usrList.end(); uiterator++)
	{
		sendUpdateToClient(*uiterator);
	}
	
	
}

void ret(queue<User*>& from, queue<User*>& to)
{
	while (!from.empty())
	{
		User* currentUser = from.front();
		from.pop();
		to.push(currentUser);
	}
}

/**
Update the db file according the data it gets from a socket.
*/
void Server::updateFile(SOCKET clientSocket)
{
	int size = Helper::getIntPartFromSocket(clientSocket, 5);
	string content = Helper::getStringPartFromSocket(clientSocket, size);
	ofstream file("data.txt");
	file << content;
}

void Server::switchUsers()
{
	
	if (_current && _current->isLoggedIn())
	{
		_users.push(_current);
		_current->setPosition(_users.size() + 2);
	}

	if (!_users.empty())
	{
		
		_current = _users.front();
		_current->setPosition(1);
		_users.pop();

		queue<User*> tmp;
		int ind = 2;
		while (!_users.empty())
		{
			_users.front()->setPosition(ind);
			tmp.push(_users.front());
			_users.pop();
			ind++;
		}

		ret(tmp, _users);
	}
	else
	{
		_current = NULL;
	}

	
	
}


/**
Input: user to disconnect.
*/
void Server::disconnect(User * user)
{
	if (user == _current)
	{
		User* tmpUser = _current;
		tmpUser->disconnect();
		switchUsers();
		delete tmpUser;
	}

	else
	{
		queue<User*> tmp;
		int ind = 2;
		while (!_users.empty())
		{
			if (_users.front() != user)
			{
				_users.front()->setPosition(ind);
				tmp.push(_users.front());
				ind++;
			}
			_users.pop();
		}
		ret(tmp, _users);
		delete user;

	}
	
}


void Server::waitToFinish(Message * message)
{
	mutex mtx;
	unique_lock<mutex> waitLcker(mtx);
	message->getCond()->wait(waitLcker);	
}


User* Server::handleSignin(Message* msg)
{
	if (this->DataBase::isUserAndPassMatch(msg))
	{
		if (this->DataBase::getUserByName(msg->getUser))
		{

		}
	}
}
