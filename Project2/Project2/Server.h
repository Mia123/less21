#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "User.h"
#include "Message.h"
#include <queue>
#include "DataBase.h"
#include <algorithm>

using namespace std;

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void accept();
	void clientHandler(SOCKET clientSocket);
	void sendUpdateToClient(User* user);
	void handleMessages();
	void sendAll();
	void updateFile(SOCKET clientSocket);
	void switchUsers();
	void disconnect(User* user);
	void waitToFinish(Message* message);

	User* handleSignin(Message*);
	bool handleSignup(Message*);
	void Server::handleSignout(User * user);
	void handleLeaveGame(Message*);
	void handleStartGame(Message*);

private:
	std::map<int, Room*> _roomsList;
	SOCKET _serverSocket;
	User* _current;
	std::queue<User*> _users;
	std::queue<Message*> _messages;
	static int _roomIdSequence;
};

void ret(queue<User*>& from, queue<User*>& to);
