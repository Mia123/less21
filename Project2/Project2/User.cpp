#include "User.h"

User::User(string name, string password,SOCKET socket) : _name(name),_password(password) _socket(socket)
{
}

User::~User()
{
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (!_currRoom)
	{
		return false;
	}
	else
	{
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		return true;
	}
}

void User::leaveRoom()
{
	if (_currRoom)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (!_currRoom)
	{
		_currRoom->closeRoom(this);
	    int id = _currRoom->getId();
		_currRoom = nullptr;
		return id;
	}
	else
	{
		return -1;
	}
}

bool User::leaveGame()
{
		
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::joinRoom(Room* newRoom)
{
	if (!_currRoom)
	{
		return false;
	}
	else
	{
		_currRoom->joinRoom(this);
		return true;
	}
}

void User::send(string message)
{
	Helper::sendData(_socket, message);
}

void User::setGame()
{

}

void User::clearGame()
{

}

string User::getName()
{
	return _name;
}

SOCKET User::getSocket()
{
	return _socket;
}

int User::getPosition()
{
	return _position;
}

bool User::isLoggedIn()
{
	return _loggedIn;
}

void User::disconnect()
{
	_loggedIn = false;
}

void User::setPosition(int position)
{
	_position = position;
}
