#pragma once
#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include "Helper.h"
#include "Room.h"

using namespace std;

class User
{
private:
	string _name;
	string _password;
	SOCKET _socket;
	int _position;
	bool _loggedIn;
	Room* _currRoom;

public:
	User(string name, string password, SOCKET socket);
	~User();
	string getName();
	void send(string message);
	void clearGame();
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();
	void setGame();
	SOCKET getSocket();
	int getPosition();
	bool isLoggedIn();
	void disconnect();
	void setPosition(int position);
}; 
